# Henism

A port of [gensim](https://www.github.com/piskvorky/gensim) from Python to Haskell

Used for _topic modelling_, _document indexing_, and _similarity retrieval_ with large corpora. Target audience is the _natural language processing_ (NLP) and _information retrieval_ (IR) communities.
