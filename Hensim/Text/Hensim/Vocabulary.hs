module Text.Hensim.Vocabulary (Vocabulary(..), TrimRule(..), TrimRuleAction(..)) where

import Data.Text (Text)
import qualified Data.Map.Strict as Map

-- Count of occurrences of a vocab word.
type Frequency = Int


-- Map of words to weights
type VocabMap = Map.Map Text Frequency

data Vocabulary = Vocabulary {
  config :: VocabConfig,
  vocabulary :: VocabMap
  }

data VocabConfig = VocabConfig {
  trimRule :: TrimRule,
  maxVocabSize :: Int
  }

-- Minimum count to keep when pruning vocab. When pruning, if a vocab
-- entry has fewer Frequency than this it is removed.
newtype MinReduce = MinReduce Int deriving (Show, Eq, Ord)

-- Trim rule to use for building a vocabulary
data TrimRule
  = Default MinReduce
  | TrimRule MinReduce CustomTrimRule

-- Action to take for a CustomTrimRule.
data TrimRuleAction =
  RuleDiscard |
  RuleKeep |
  RuleDefault

type CustomTrimRule = Text -> Frequency -> MinReduce -> TrimRuleAction

buildVocab :: VocabConfig -> [Text]  -> Vocabulary
buildVocab config sentences =
    scanVocab (Vocabulary config Map.empty) sentences

scanVocab :: Vocabulary -> [Text] -> Vocabulary
scanVocab (Vocabulary config vocab) words =
  let
    tr = trimRule config
    incWord Nothing = Just 1
    incWord (Just n) = Just (n + 1)
    mvs = maxVocabSize config

    alterWord :: VocabMap -> Text -> VocabMap
    alterWord vocab w =
      pruneVocab config $ Map.alter incWord w vocab
    voc = foldl alterWord vocab words
  in
    Vocabulary config voc

pruneVocab :: VocabConfig -> VocabMap -> VocabMap
pruneVocab config vocab =
    if Map.size vocab > (maxVocabSize config) then pruneVocab' config vocab
    else vocab

pruneVocab' :: VocabConfig -> VocabMap -> VocabMap
pruneVocab' config vocab =
  Map.filterWithKey (keepVocabItem $ trimRule config) vocab

keepVocabItem :: TrimRule -> Text -> Int -> Bool
keepVocabItem (Default (MinReduce minCount)) word count =
  count >= minCount
keepVocabItem (TrimRule (MinReduce minCount) rule) word count =
  case rule word count (MinReduce minCount) of
  RuleDiscard -> False
  RuleKeep -> True
  RuleDefault -> keepVocabItem (Default (MinReduce minCount)) word count
